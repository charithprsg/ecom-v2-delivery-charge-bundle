<?php

namespace Esol\DeliveryChargeBundle\Repository;

use Esol\DeliveryChargeBundle\Entity\CourierOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourierOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourierOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourierOption[]    findAll()
 * @method CourierOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourierOptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourierOption::class);
    }

    // /**
    //  * @return CourierOption[] Returns an array of CourierOption objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourierOption
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
